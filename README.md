# PoopTs

"just poop your notes ... via TypeScript frontend"

This webapp does allow you to self-host a pinbord of cards

## How To Start

1. Install git, docker, docker-compose
2. Clone this repo
3. Run `docker-compose build` to create container images
4. Copy `.env.demo` to `.env` and change values
5. Run `docker-compose up`

## Vision and Implementation

The Vision is a self-hosted web service, that let you manage "PostIt"-like cards.
It's aimed to be like a public blackboard for single person or single team.

In terms of the [GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation),
it's required to minimize all data that a system possess and store down to only the required one.
So the permission is flat in any case, a user-management was explicit left out and replaced with a simple token.
The token can be changed at any time without any regressions.

## Architecture

### Container

- **Database**: Run a DBMS, container image provided externally
- **Backend**: Interface with Database, container is build from the PHP backend
- **Frontend**: Interface with Backend, container is build from the Svelte App and servse it as static content. It also proxy the API-Requests to the Backend.

### Module Dependency Graph

```mermaid
  graph LR;
      subgraph Database
      DBMS
      end
      subgraph Backend
      API-->Card-Service;
      API-->TokenAuth;
      Card-Service-->Card-Repository;
      Card-Repository-->DbConnection;
      DbConnection-->DBMS;
      end
      subgraph Frontend
      WebUI-->Card-Service-Client;
      Card-Service-Client-->API;
      end
      Browser-->WebUI
```

### Technologies

The **Backend** is a PHP request that expose the Database via a simple REST-API.
It sanitizes and validate all requests and auth headers.

The **Frontend** use a simple REST-Client implemented asyncron via [ky](https://github.com/sindresorhus/ky) and visualize it via a Singlepage-App written in [Svelte](https://svelte.dev/). For styling the [Tailwind CSS framework](https://tailwindcss.com/) easy to read and maintain.

## API

The API is defined by the [OpenAPI](https://swagger.io/specification/) [Definition](./openapi/index.yaml) read more [here](./openapi/README.md).
The current version is under the prefix path `/v1/`, so when there is a breaking change in the backend it won't conflict. You can even implement a completely new Backend in a separate container and still can run both and migrate API consumer piece by piece.
