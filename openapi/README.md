# OpenAPI

## Rest Methods Defined

```none
GET    - return obj           - status code 200
POST   - create new obj       - status code 201
DELETE - remove obj           - status code 204
PATCH  - edit existing obj    - status code 200
PUT    - assign existing obj  - status code 200
```

## Tools

- [editor-next.swagger.io](https://editor-next.swagger.io): to test / edit interactively
