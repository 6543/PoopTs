import { writable } from 'svelte/store';

export const loginToken = writable('');
export const cards = writable([]);
export const searchTag = writable('');
export const searchQuery = writable('');
