import type CardModel from '../interfaces/card-model.interface';
import ky from 'ky';

const apiEndpoint = '/api/v1';

class CardService {
	loginToken: string;

	async login(token: string): Promise<boolean> {
		try {
			await ky
				.get('cards', {
					prefixUrl: apiEndpoint,
					headers: {
						'content-type': 'application/json',
						token: token
					}
				})
				.json();
		} catch (error) {
			console.log('login failed with following token:', token);
			console.error(error);

			return await new Promise((resolve) => resolve(false));
		}

		this.loginToken = token;
		console.log('logged in with following token:', token);
		return await new Promise((resolve) => resolve(true));
	}

	async getCards(): Promise<CardModel[]> {
		return ky
			.get('cards', {
				prefixUrl: apiEndpoint,
				headers: {
					'content-type': 'application/json',
					token: this.loginToken
				}
			})
			.json();
	}

	async search(query: string, tag: string): Promise<CardModel[]> {
		return ky
			.get('cards?query=' + encodeURI(query) + '&tag=' + encodeURI(tag), {
				prefixUrl: apiEndpoint,
				headers: {
					'content-type': 'application/json',
					token: this.loginToken
				}
			})
			.json();
	}

	async create(card: Omit<CardModel, 'id'>): Promise<CardModel> {
		return ky
			.post('cards', {
				prefixUrl: apiEndpoint,
				headers: {
					'content-type': 'application/json',
					token: this.loginToken
				},
				json: card
			})
			.json();
	}

	async update(card: CardModel): Promise<CardModel> {
		return ky
			.patch('cards/' + card.id, {
				prefixUrl: apiEndpoint,
				headers: {
					'content-type': 'application/json',
					token: this.loginToken
				},
				json: card
			})
			.json();
	}

	async delete(cardId: number): Promise<boolean> {
		try {
			await ky.delete('cards/' + cardId, {
				prefixUrl: apiEndpoint,
				headers: {
					token: this.loginToken
				}
			});
		} catch (error) {
			console.log('could not delete card wit id: ' + cardId);
			console.error(error);
			return await new Promise((resolve) => resolve(false));
		}
		return await new Promise((resolve) => resolve(true));
	}
}

export const cardService = new CardService();
