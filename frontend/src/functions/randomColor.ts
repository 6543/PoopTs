// got this from https://www.cluemediator.com/create-a-random-color-based-on-a-string-using-javascript and modified it a little bit
export const getRandomColor = (name: string): string => {
	// get first alphabet in upper case
	const firstAlphabet = name.charAt(0).toLowerCase();

	// get the ASCII code of the character
	const asciiCode = firstAlphabet.charCodeAt(0);

	// number that contains 3 times ASCII value of character -- unique for every alphabet
	const colorNum = asciiCode.toString() + asciiCode.toString() + asciiCode.toString();

	const num = Math.round(0xffffff * parseInt(colorNum));
	const r = (num >> 16) & 255;
	const g = (num >> 8) & 255;
	const b = num & 255;

	return `rgb(${r}, ${g}, ${b}, 0.3)`;
};
