export default interface CardModel {
	id: number;
	title: string;
	tags: string[];
	content: string; // should be markdown
}
