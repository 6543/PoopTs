<?php

declare(strict_types=1);

namespace Poopts\Backend\Http;

use Poopts\Backend\Http\RequestInterface;

class Request implements RequestInterface
{
    /**
     * @var string
     */
    protected string $url = '';

    /**
     * @var string
     */
    protected string $method = '';

    /**
     * @var array
     */
    protected array $parameters = [];

    /**
     * @var string
     */
    protected string $body = '';

    /**
     * @var array
     */
    protected array $headers = [];

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    protected function setUrl($url): void
    {
        $this->url = strtolower($url);
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param string $name
     * @return string
     */
    public function getParameter(string $name): string|null
    {
        if (array_key_exists($name, $this->parameters)) {
            return $this->parameters[$name];
        }

        return null;
    }

    /**
     * @param string $name
     * @param string $parameter
     */
    protected function setParameter(string $name, string $parameter): void
    {
        $this->parameters[strtolower($name)] = $parameter;
    }

    /**
     * @param array $parameters
     */
    protected function setParameters(array $parameters): void
    {
        foreach ($parameters as $key => $value) {
            if ($key != '' && $value != '') {
                $this->setParameter($key, $value);
            }
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasParameter(string $name): bool
    {
        return isset($this->parameters[strtolower($name)]);
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    protected function setMethod($method): void
    {
        $this->method = strtoupper($method);
    }

  /**
   * @return string
   */
  public function getBody(): string
  {
      return $this->body;
  }

  /**
   * @param string $body
   */
  protected function setBody($body): void
  {
      $this->body = $body;
  }

  /**
   * @return array
   */
  public function getHeaders(): array
  {
      return $this->headers;
  }

  /**
   * @param string $name
   * @return string|null
   */
  public function getHeader(string $name): string | null
  {
      $name = strtolower($name);
      if (!array_key_exists($name, $this->headers)) {
          return null;
      }
      return $this->headers[$name];
  }

  /**
   * @param array $headers
   */
  public function setHeaders(array $headers): void
  {
      foreach ($headers as $name => $value) {
          $this->headers[strtolower($name)] = $value;
      }
  }

  public function __construct()
  {
      $url = $_SERVER['REQUEST_URI'];
      $this->setUrl(parse_url($url, PHP_URL_PATH));
      $this->setMethod($_SERVER['REQUEST_METHOD']);
      $query = parse_url($url, PHP_URL_QUERY);
      if ($query != null) {
          $queryParts = explode('&', trim(parse_url($url, PHP_URL_QUERY), '&'));
          for ($i = 0; $i < count($queryParts); $i++) {
              $queryPart = explode('=', $queryParts[$i]);
              if (count($queryPart) == 2) {
                  $this->setParameter($queryPart[0], $queryPart[1]);
              }
          }
      }
      $this->setBody(file_get_contents("php://input"));
      $this->setHeaders(getallheaders());
  }
}
