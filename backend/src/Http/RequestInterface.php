<?php

declare(strict_types=1);

namespace Poopts\Backend\Http;

interface RequestInterface
{
    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @return array
     */
    public function getParameters(): array;

    /**
     * @param string $name
     * @return string|null
     */
    public function getParameter(string $name): string|null;

    /**
     * @param string $name
     * @return bool
     */
    public function hasParameter(string $name): bool;

    /**
     * @return string
     */
    public function getMethod(): string;

    /**
     * @return string
     */
    public function getBody(): string;

    /**
     * @return array
     */
    public function getHeaders(): array;

    /**
     * @param string $name
     * @return string
     */
    public function getHeader(string $name): string | null;
}
