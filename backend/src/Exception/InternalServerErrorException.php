<?php

declare(strict_types=1);

namespace Poopts\Backend\Exception;

use Exception;
use Throwable;

class InternalServerErrorException extends Exception
{
    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return 500;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return 'InternalServerError';
    }

    public function __construct(Throwable $previous = null)
    {
        $this->getMessage();
        parent::__construct($this->errorMessage(), 1, $previous);
    }
}
