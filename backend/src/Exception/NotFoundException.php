<?php

declare(strict_types=1);

namespace Poopts\Backend\Exception;

use Exception;
use Throwable;

class NotFoundException extends Exception
{
    /**
     * @var string
     */
    private $msg;

    /**
     * @param string $msg
     */
    public function __construct(string $msg, Throwable $previous = null)
    {
        $this->msg = $msg;
        parent::__construct($msg, $this->getStatusCode(), $previous);
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return 404;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return $this->msg;
    }
}
