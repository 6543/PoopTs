<?php

declare(strict_types=1);

namespace Poopts\Backend\Exception;

use Exception;
use Throwable;

class ErrorException extends Exception
{
    /**
     * @var string
     */
    private string $msg;

    /**
     * @var int
     */
    private int $status;

    /**
     * @param int $status
     * @param string $msg
     */
    public function __construct(int $status, string $msg, Throwable $previous = null)
    {
        $this->status = $status;
        $this->msg = $msg;
        parent::__construct($msg, $status, $previous);
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return $this->msg;
    }
}
