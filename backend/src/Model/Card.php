<?php

declare(strict_types=1);

namespace Poopts\Backend\Model;

class Card implements \JsonSerializable
{
    /**
     * @var int
     */
    public int $id = 0;

    /**
     * @var string
     */
    public string $title = '';

    /**
     * @var string
     */
    public string $tags = '';

    /**
     * @var string
     */
    public string $content = '';


    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        $result['id'] = $this->id;
        $result['title'] = $this->title;
        $result['tags'] = $this->getTags();
        $result['content'] = $this->content;
        return $result;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        if ($this->tags == null || $this->tags == '') {
            return [];
        }
        return unserialize($this->tags);
    }

    /**
     * @param array $tags
     */
    public function setTags(array $tags): void
    {
        $this->tags = serialize($tags);
    }

    /**
    * @param string $json
    * @return Card
    */
    public static function Deserialize(string $json): Card
    {
        $card = new Card();
        $decoded = json_decode($json);

        foreach ($decoded as $key => $value) {
            if (!property_exists($card, $key)) {
                continue;
            }

            if ($key == 'tags') {
                $card->setTags($value);
                continue;
            }

            $card->{$key} = $value;
        }

        return $card;
    }
}
