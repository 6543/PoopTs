<?php

declare(strict_types=1);

namespace Poopts\Backend;

use Poopts\Backend\Exception\NotFoundException;
use Poopts\Backend\Exception\InternalServerErrorException;
use Poopts\Backend\Service\CardService;
use Poopts\Backend\Http\RequestInterface;
use Poopts\Backend\Http\ResponseInterface;
use Poopts\Backend\TokenAuth;

class Api
{
    /**
     * handle request by select the service and method based on urlPath
     *
     * @param RequestInterface $req
     * @param ResponseInterface $resp
     *
     * @throws \Exception
     */
    public function handle(RequestInterface $req, ResponseInterface $resp): void
    {
        $urlParts = explode('/', trim($req->getUrl(), '/'));
        if (count($urlParts) < 3) {
            throw new NotFoundException('to less path segments');
        }

        // check and remove api and version prefix
        if ($urlParts[0] != 'api' || $urlParts[1] != 'v1') {
            throw new NotFoundException('no api v1 request detected');
        }

        $tokenAuth = new TokenAuth();

        // handle all api endpoints
        switch ($urlParts[2]) {
            case 'cards':
                $cardService = new CardService();

                switch ($req->getMethod()) {
                    case 'GET':
                        $tokenAuth->checkAuth($req, $resp, false);
                        $cardService->list($req, $resp);
                        break;
                    case 'POST':
                        $tokenAuth->checkAuth($req, $resp, true);
                        $cardService->create($req, $resp);
                        break;
                    case 'PATCH':
                        $tokenAuth->checkAuth($req, $resp, true);
                        $cardService->update($req, $resp);
                        break;
                    case 'DELETE':
                        $tokenAuth->checkAuth($req, $resp, true);
                        $cardService->delete($req, $resp);
                        break;
                    case 'OPTIONS':
                    case 'HEAD':
                        $resp->setBody('');
                        $resp->setHeader('allow', 'GET, POST, PATCH, DELETE, OPTIONS');
                        $resp->setStatusCode(204);
                        break;
                    default:
                        throw new InternalServerErrorException();
                }
                break;
            default:
                throw new NotFoundException('api endpoint not found');
        }
    }
}
