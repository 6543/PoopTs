<?php

declare(strict_types=1);

namespace Poopts\Backend;

use Exception;
use Poopts\Backend\Exception\ErrorException;
use Poopts\Backend\Exception\InternalServerErrorException;
use Poopts\Backend\Http\RequestInterface;
use Poopts\Backend\Http\ResponseInterface;

class TokenAuth
{
    /**
     * @var string
     */
    private string $token = '';

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $token = getenv('EDIT_TOKEN');
        if (is_bool($token)) {
            error_log('env var "EDIT_TOKEN" not set and needed');
            throw new  InternalServerErrorException('EDIT_TOKEN missing');
        }
        $this->token = $token;
    }

      /**
     * handle request by select the service and method based on urlPath
     *
     * @param RequestInterface $req
     * @param ResponseInterface $resp
     * @param bool $required
     *
     * @throws Exception
     */
      public function checkAuth(RequestInterface $req, ResponseInterface $resp, bool $required): void
      {
          $reqToken = $req->getHeader('token');

          if ($reqToken == null) {
              if (!$required) {
                  // well it's not required and we have no token
                  return;
              } else {
                  throw new ErrorException(403, 'required token not set');
              }
          }

          if ($reqToken != $this->token) {
              throw new ErrorException(403, 'wrong token');
          }
      }
}
