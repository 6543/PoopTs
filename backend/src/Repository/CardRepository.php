<?php

declare(strict_types=1);

namespace Poopts\Backend\Repository;

use Poopts\Backend\DbConnection;
use Poopts\Backend\Model\Card;
use PDO;

class CardRepository
{
    /**
     * @var PDO
     */
    protected PDO $connection;

    public function __construct()
    {
        $connector = new DbConnection();
        $connector->connect();
        $this->connection = $connector->getConnection();
    }

    /**
     * @param Card $card
     * @return Card|null
     */
    public function insert(Card $card): ?Card
    {
        $query = $this->connection->prepare(
            'INSERT INTO cards (title, tags, content) VALUES (:title, :tags, :content);'
        );
        $query->bindParam(':title', $card->title);
        $query->bindParam(':tags', $card->tags);
        $query->bindParam(':content', $card->content);
        $query->execute();

        $query = $this->connection->prepare(
            'SELECT id, title, tags, content FROM cards WHERE title LIKE :title AND content LIKE :content;'
        );
        $query->bindParam(':title', $card->title);
        $query->bindParam(':content', $card->content);
        $query->execute();
        $query->setFetchMode(PDO::FETCH_CLASS, Card::class);
        return $query->fetch();
    }

    public function update(Card $card): ?Card
    {
        $query = $this->connection->prepare(
            'UPDATE cards set title = :title, tags = :tags, content = :content WHERE id = :id;'
        );
        $query->bindParam(':id', $card->id);
        $query->bindParam(':title', $card->title);
        $query->bindParam(':tags', $card->tags);
        $query->bindParam(':content', $card->content);
        $query->execute();

        $query = $this->connection->prepare(
            'SELECT id, title, tags, content FROM cards WHERE id = :id;'
        );
        $query->bindParam(':id', $card->id);
        $query->execute();
        $query->setFetchMode(PDO::FETCH_CLASS, Card::class);
        return $query->fetch();
    }

    public function deleteById(int $id): void
    {
        $query = $this->connection->prepare(
            'DELETE FROM cards WHERE id = :id;'
        );
        $query->bindParam(':id', $id);
        $query->execute();
    }

    public function find(string|null $search, string|null $tag): array
    {
        if ($search != null && $search != '') {
            $search = '%' . $search . '%';
        } else {
            $search = '';
        }
        if ($tag != null && $tag != '') {
            $tag = '%"' . $tag . '"%';
        } else {
            $tag = '';
        }


        $sql = 'SELECT id, title, tags, content FROM cards';

        $where = '';
        error_log(var_export($tag, true));
        if ($search != '') {
            $where = '(title LIKE :search OR content LIKE :search)';
        }
        if ($tag != '') {
            if ($where != '') {
                $where = $where . ' AND ';
            }
            $where = $where . 'tags LIKE :tag';
        }

        if ($where != '') {
            $sql = $sql . ' WHERE ' . $where;
        }

        $query = $this->connection->prepare($sql);
        if ($search != '') {
            $query->bindParam(':search', $search);
        }
        if ($tag != '') {
            $query->bindParam(':tag', $tag);
        }
        error_log(var_export($query, true));

        $query->execute();
        $query->setFetchMode(PDO::FETCH_CLASS, Card::class);
        return $query->fetchAll();
    }
}
