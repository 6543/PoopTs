<?php

declare(strict_types=1);

namespace Poopts\Backend\Service;

use Poopts\Backend\Exception\NotFoundException;
use Poopts\Backend\Model\Card;
use Poopts\Backend\Exception\ErrorException;
use Poopts\Backend\Http\RequestInterface;
use Poopts\Backend\Http\ResponseInterface;
use Poopts\Backend\Repository\CardRepository;

class CardService
{
    private CardRepository $repo;

    public function __construct()
    {
        $this->repo = new CardRepository();
    }

    public function list(RequestInterface $request, ResponseInterface $response): void
    {
        $query = $request->getParameter('query');
        $tag = $request->getParameter('tag');

        $cardList = $this->repo->find($query, $tag);

        $response->setBody(json_encode($cardList));
        $response->setStatusCode(200);
    }

    public function create(RequestInterface $request, ResponseInterface $response): void
    {
        // deserialize & sanitize
        $cardFromBody = Card::Deserialize($request->getBody());
        $cardFromBody->id = 0;

        // validate
        if ($cardFromBody->title == '' || $cardFromBody->content == '') {
            throw new ErrorException(400, 'card not valid');
        }

        // persistent
        $newCard = $this->repo->insert($cardFromBody);

        // render result
        $response->setBody(json_encode($newCard));
        $response->setStatusCode(201);
    }

    public function update(RequestInterface $request, ResponseInterface $response): void
    {
        $urlParts = explode('/', trim($request->getUrl(), '/'));
        if (count($urlParts) < 4) {
            throw new ErrorException(400, 'id is missing');
        }
        $id = (int) $urlParts[3];

        // deserialize & sanitize
        $cardFromBody = Card::Deserialize($request->getBody());
        $cardFromBody->id = $id;

        // validate
        if ($cardFromBody->title == '' || $cardFromBody->content == '') {
            throw new ErrorException(400, 'card not valid');
        }

        // persistent
        $updatedCard = $this->repo->update($cardFromBody);
        if ($updatedCard == null) {
            throw new NotFoundException('card not found');
        }

        // render result
        $response->setBody(json_encode($updatedCard));
        $response->setStatusCode(200);
    }

    public function delete(RequestInterface $request, ResponseInterface $response): void
    {
        $urlParts = explode('/', trim($request->getUrl(), '/'));
        if (count($urlParts) < 4) {
            throw new ErrorException(400, 'id is missing');
        }
        $id = (int) $urlParts[3];

        // persistent
        $this->repo->deleteById($id);

        $response->setBody('{}');
        $response->setStatusCode(204);
    }
}
