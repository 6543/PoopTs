<?php

declare(strict_types=1);

namespace Poopts\Backend;

use PDO;

class DbConnection
{
    /**
     * @var PDO
     */
    protected PDO $connection;

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->connection;
    }

    /**
     * @return void
     *
     * @throws \PDOException
     */
    public function connect(): void
    {
        $this->connection = new \PDO(
            getenv('DB_DSN'),
            getenv('DB_USER'),
            getenv('DB_PASSWORD')
        );
        // set the PDO error mode to exception
        $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }
}
