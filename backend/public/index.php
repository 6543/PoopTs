<?php

declare(strict_types=1);

// include composer based autoload
require_once(__DIR__ . '/../vendor/autoload.php');

use Poopts\Backend\Http\Request;
use Poopts\Backend\Http\Response;
use Poopts\Backend\Exception\NotFoundException;
use Poopts\Backend\Exception\InternalServerErrorException;
use Poopts\Backend\Exception\ErrorException;
use Poopts\Backend\Api;

## init
$req = new Request();
$resp = new Response();
error_log(var_export($req, true));

try {
    # try to handle request ...
    $api = new Api();
    $api->handle($req, $resp);
    ## ... and handle errors
} catch (NotFoundException $exception) {
    $resp->setStatusCode($exception->getStatusCode());
    $resp->setBody($exception->errorMessage());
} catch (InternalServerErrorException $exception) {
    $resp->setStatusCode($exception->getStatusCode());
    $resp->setBody($exception->errorMessage());
} catch (ErrorException $exception) {
    $resp->setStatusCode($exception->getStatusCode());
    $resp->setBody($exception->errorMessage());
} catch (\Exception $exception) {
    error_log(var_export($exception, true));
    $resp->setStatusCode(500);
    $resp->setBody('InternalServerError');
}

## write respond
http_response_code($resp->getStatusCode());
foreach ($resp->getHeaders() as $key => $value) {
    header($key . ': ' . $value);
}
echo $resp->getBody();
